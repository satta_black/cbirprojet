from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from . import data
import cv2


def home_view(request):
    return render(request, 'accueil.html')

def combine_view(request):
    return render(request, 'extractionCombine.html')

def simple_view(request):
    return render(request, 'extractionSimple.html', context={'reponse': 'Faux'})


def w_file(file_obj):
    new_name = 'queryimage.jpg'
    f = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + new_name + "", 'wb')
    for line in file_obj.chunks():
        f.write(line)
    f.close()

def F_upload(request):
    if request.method == "GET":
        return render(request, 'extractionSimple.html')
    else:
        f = request.FILES
        print("form the uploaded file is:", f)
        f_obj = f.get("uploadfile")

        n = f_obj.name
        s = f_obj.size
        distance = request.POST.get('distance','')
        methode = request.POST.get('methode', '')
        nombre = request.POST.get('nombre', '')

        print("The file name is:", n)
        print("The file size is:", s)

        w_file(f_obj)

        img = cv2.imread('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/queryimage.jpg', 0)

        if (methode == "haralick" and distance == "eucledienne"):
            images = data.haralickEuc(img, nombre)
        elif (methode == "haralick" and distance == "canberra"):
            images = data.haralickCan(img, nombre)
        elif (methode == "haralick" and distance == "chebyshev"):
            images = data.haralickChe(img, nombre)
        elif (methode == "haralick" and distance == "manathan"):
            images = data.haralickMan(img, nombre)
        elif (methode == "GLCM" and distance == "eucledienne"):
            images = data.GLCMEuc(img, nombre)
        elif (methode == "GLCM" and distance == "canberra"):
            images = data.GLCMCan(img, nombre)
        elif (methode == "GLCM" and distance == "manathan"):
            images = data.GLCMMan(img, nombre)
        elif (methode == "GLCM" and distance == "chebyshev"):
            images = data.GLCMChe(img, nombre)
        elif (methode == "bit" and distance == "eucledienne"):
            images = data.bitEuc(img, nombre)
        elif (methode == "bit" and distance == "canberra"):
            images = data.bitCan(img, nombre)
        elif (methode == "bit" and distance == "manathan"):
            images = data.bitMan(img, nombre)
        elif (methode == "bit" and distance == "chebyshev"):
            images = data.bitChe(img, nombre)
        elif (methode == "bitHaralick" and distance == "eucledienne"):
            images = data.bitHaralickEuc(img, nombre)
        elif (methode == "bitHaralick" and distance == "canberra"):
            images = data.bitHaralickCan(img, nombre)
        elif (methode == "bitHaralick" and distance == "manathan"):
            images = data.bitHaralickMan(img, nombre)
        elif (methode == "bitHaralick" and distance == "chebyshev"):
            images = data.bitHaralickChe(img, nombre)
        elif (methode == "bitGLCM" and distance == "eucledienne"):
            images = data.bitGLCMEuc(img, nombre)
        elif (methode == "bitGLCM" and distance == "canberra"):
            images = data.bitGLCMCan(img, nombre)
        elif (methode == "bitGLCM" and distance == "manathan"):
            images = data.bitGLCMMan(img, nombre)
        elif (methode == "bitGLCM" and distance == "chebyshev"):
            images = data.bitGLCMChe(img, nombre)
        else:
            images = []
        pourc = images[0]['pourcentage']
        nbCovid = images[0]['nbCovid']
        # print(images)
        return render(request, 'extractionSimple.html', context={'reponse': 'Vrai', 'Images': images, 'distance':distance, 'methode':methode, 'nombre' : nombre, 'pourc': pourc, 'nbCovid':nbCovid})



