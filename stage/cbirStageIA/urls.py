from django.contrib import admin
from django.urls import path, include
from django.conf import settings #add this
from django.conf.urls.static import static #add this
from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path("", views.home_view, name='home'),
    path("accueil/", views.home_view, name='home'),
    path("extraction/combine/", views.combine_view, name='combine'),
    path("extraction/simple/", views.simple_view, name='simple'),
    path('uploadT', views.F_upload, name='uploadT')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
