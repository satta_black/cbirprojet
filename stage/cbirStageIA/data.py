import cv2,csv
from mahotas import features
from os import listdir
from typing import List
from scipy.spatial import distance
import pandas as pd
import numpy as np
from scipy.spatial import distance
from skimage.feature import graycomatrix, graycoprops
from BiT import bio_taxo


def haralickEuc(img, nombre):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickS.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


#

def haralickCan(img, nombre):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickS.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


#

def haralickChe(img, nombre):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickS.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


#

def haralickMan(img, nombre):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickS.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2

    return listTotal2

#

def GLCMEuc(img, nombre):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmS.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2

#

def GLCMCan(img, nombre):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmS.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2



def GLCMChe(img, nombre):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmS.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2



def GLCMMan(img, nombre):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmS.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitEuc(img, nombre):
    haralick_features = bio_taxo(img)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/Bit.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2

def bitCan(img, nombre):
    haralick_features = bio_taxo(img)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/Bit.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitChe(img, nombre):
    haralick_features = bio_taxo(img)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/Bit.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitMan(img, nombre):
    haralick_features = bio_taxo(img)

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/Bit.csv', 'r')
    o = csv.reader(c)
    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitHaralickEuc(img, nombre):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitHaralick.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitHaralickCan(img, nombre):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitHaralick.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitHaralickChe(img, nombre):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitHaralick.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitHaralickMan(img, nombre):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitHaralick.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitGLCMEuc(img, nombre):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitGlcm.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitGLCMCan(img, nombre):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitGlcm.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2

def bitGLCMChe(img, nombre):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitGlcm.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2


def bitGLCMMan(img, nombre):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = bio_taxo(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    c = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/bitGlcm.csv', 'r')
    o = csv.reader(c)

    listTotal = []
    listTotal2 = []

    for r in o:
        listD = r[0]
        listN = r[1]
        list = r[2:]
        float_list = []

        for item in list:
            float_list.append(float(item))

        if (listD == "CT_COVID" or listD == "CT_NonCOVID"):
            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'image/' + listD + '/' + listN]

            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)
    c.close()

    def get_dist(listTotal):
        return listTotal.get('distance')

    listTotal.sort(key=get_dist)
    nb = 0

    for i in range(0, int(nombre)):
        if (listTotal[i]['dossier'] == "CT_COVID"):
            nb = nb + 1

    print(nb)
    print(nombre)
    pourc = float(nb) * 100 / float(nombre)

    for i in range(0, int(nombre)):
        key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
        valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                        nb, pourc]
        dictio2 = dict(zip(key_list2, valeur_list2))
        listTotal2.append(dictio2)
    return listTotal2